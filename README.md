# [dice](https://gitlab.com/eidoom/dice)

`./dist.py` (`h` to show usage) calculates the roll outcome probabilities for combinations of polyhedral dice.
