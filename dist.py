#!/usr/bin/env python3

import collections
import math
import random
import sys

import matplotlib.pyplot


PHI = (1 + math.sqrt(5)) / 2


def get_dice():
    dice_str = sys.argv[1]

    try:
        # for advantage and disadvantage
        # nb strictly apply to d20 rolls only
        mod = sys.argv[2][0].lower()
    except IndexError:
        mod = None

    dice = collections.Counter()
    bonus = 0
    for die in dice_str.split("+"):
        if "d" in die:
            number, faces = map(int, die.split("d"))
            dice[faces] += number
        else:
            bonus += int(die)

    return dice, bonus, mod


def get_roll(dice):
    return sum(
        random.randint(1, faces)
        for faces, number in dice.items()
        for _ in range(number)
    )


def get_rolls(dice, bonus, mod):
    """
    lazy way to estimate combinations
    """
    repeats = 100000

    rolls = []
    for _ in range(repeats):
        match mod:
            case "a":
                roll = max(get_roll(dice) for _ in range(2))
            case "d":
                roll = min(get_roll(dice) for _ in range(2))
            case _:
                roll = get_roll(dice)

        rolls.append(roll + bonus)

    return rolls


def stats(rolls):
    mini = min(rolls)
    maxi = max(rolls)
    print("min", mini)
    print("max", maxi)

    n = len(rolls)
    mean = round(sum(rolls) / n, 1)
    print(f"mean: {mean}")

    bins = range(mini, maxi + 2)
    weights = [1 / n for _ in range(n)]

    size = 6.4
    fig, ax = matplotlib.pyplot.subplots(
        layout="constrained", figsize=(size, size / PHI)
    )

    ax.set_xticks(bins[:-1])
    ax.set_ylabel("Probablility to roll target or above")
    ax.set_xlabel("Outcome")

    ax.hist(rolls, bins=bins, weights=weights, align="left", cumulative=-1)

    fig.savefig("output.pdf", bbox_inches="tight")


if __name__ == "__main__":
    if sys.argv[1] in ("-h", "--help", "h", "help"):
        print(
            """usage examples:
./dist.py 1d20      # die
./dist.py 2d20+3d12 # dice
./dist.py d20 a     # advantage
./dist.py d20 d     # disadvantage"""
        )
        exit()

    dice, bonus, mod = get_dice()

    rolls = get_rolls(dice, bonus, mod)

    stats(rolls)
